-- Create a new database with an appropriate domain-related name
CREATE DATABASE EmployeeManagement;
USE EmployeeManagement;

-- Create a schema within the database
CREATE SCHEMA HR;

-- Create the Departments table
CREATE TABLE HR.Departments (
    DepartmentID INT PRIMARY KEY,
    DepartmentName VARCHAR(50) NOT NULL,
    Location VARCHAR(100) NOT NULL,
    CONSTRAINT CHK_Location CHECK (Location IN ('New York', 'San Francisco', 'Los Angeles'))
);

-- Insert sample data into the Departments table
INSERT INTO HR.Departments (DepartmentID, DepartmentName, Location)
VALUES (1, 'Human Resources', 'New York'),
       (2, 'Finance', 'San Francisco');

-- Create the Employees table
CREATE TABLE HR.Employees (
    EmployeeID INT PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    Gender ENUM('Male', 'Female', 'Other') NOT NULL,
    BirthDate DATE NOT NULL,
    HireDate DATE NOT NULL,
    DepartmentID INT,
    record_ts DATE DEFAULT CURRENT_DATE,
    CONSTRAINT FK_Department_Employee FOREIGN KEY (DepartmentID) REFERENCES HR.Departments(DepartmentID)
);

-- Insert sample data into the Employees table
INSERT INTO HR.Employees (EmployeeID, FirstName, LastName, Gender, BirthDate, HireDate, DepartmentID)
VALUES (1, 'John', 'Doe', 'Male', '1980-01-15', '2005-03-20', 1),
       (2, 'Jane', 'Smith', 'Female', '1990-07-10', '2010-05-15', 2);

-- Create the Salaries table
CREATE TABLE HR.Salaries (
    SalaryID INT PRIMARY KEY,
    EmployeeID INT,
    Salary DECIMAL(10, 2) NOT NULL,
    record_ts DATE DEFAULT CURRENT_DATE,
    CONSTRAINT CHK_Salary CHECK (Salary >= 0),
    CONSTRAINT FK_Employee_Salary FOREIGN KEY (EmployeeID) REFERENCES HR.Employees(EmployeeID)
);

-- Insert sample data into the Salaries table
INSERT INTO HR.Salaries (SalaryID, EmployeeID, Salary)
VALUES (1, 1, 75000.00),
       (2, 2, 85000.00);

-- Add 'record_ts' field to the Employees table
ALTER TABLE HR.Employees
ADD record_ts DATE DEFAULT CURRENT_DATE;

-- Add 'record_ts' field to the Departments table
ALTER TABLE HR.Departments
ADD record_ts DATE DEFAULT CURRENT_DATE;

-- Add 'record_ts' field to the Salaries table
ALTER TABLE HR.Salaries
ADD record_ts DATE DEFAULT CURRENT_DATE;
